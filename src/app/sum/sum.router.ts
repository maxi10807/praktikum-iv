import { Router } from "express";

export const router = Router();

router.get("/sum/:a/:b", (req, res) => {
  const a = Number(req.params.a);
  const b = Number(req.params.b);

  const sum = a + b;
  let response = "Sum: " + sum;

  res.status(200).send({
    msg: response,
    result: sum,
  });
});

